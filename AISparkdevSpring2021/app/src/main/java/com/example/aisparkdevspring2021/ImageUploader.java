package com.example.aisparkdevspring2021;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.normal.TedPermission;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ImageUploader extends AppCompatActivity implements View.OnClickListener {


    private ImageButton image1, image2, image3;
    private ImageButton image4, image5, image6;
    private Button upload;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private FirebaseAuth mAuth;
    private ArrayList<Uri> imageViews;
    private ActivityResultLauncher<Intent> bioResultLauncher;
    PermissionListener permissionlistener;
    ActivityResultLauncher<String> requestPermissionLauncher;



    private ActivityResultLauncher<Intent> launcher1, launcher2, launcher3, launcher4, launcher5, launcher6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageupload);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        mAuth = FirebaseAuth.getInstance();
        imageViews = new ArrayList<>();

        upload = findViewById(R.id.upload);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        image6 = findViewById(R.id.image6);

        image1.setOnClickListener(this);
        image2.setOnClickListener(this);
        image3.setOnClickListener(this);
        image4.setOnClickListener(this);
        image5.setOnClickListener(this);
        image6.setOnClickListener(this);
        upload.setOnClickListener(this);


//        permissionlistener = new PermissionListener() {
//            @Override
//            public void onPermissionGranted() {
//                Toast.makeText(ImageUploader.this, "Permission Granted", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onPermissionDenied(List<String> deniedPermissions) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//
//                builder.setMessage("App will now return to the login screen.");
//
//                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        setResult(RESULT_CANCELED);
//                        finish();
//                    }
//                });
//            }
//        };
//
//        TedPermission.create()
//                .setPermissionListener(permissionlistener)
//                .setDeniedMessage("If you reject permission,you can not create a new account\n\nPlease turn on permissions at [Setting] > [Permission]")
//                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
//                .check();

        requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
            if (isGranted) {
                Toast.makeText(this, "Permission granted! Thank you!", Toast.LENGTH_SHORT);
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        });





        bioResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == RESULT_OK){
                    Intent data = result.getData();

                    Data d = (Data) data.getSerializableExtra("data");
                    getIntent().removeExtra("data");
                    getIntent().putExtra("data", d);
                    getIntent().putExtra("imageURIs", imageViews);

                    setResult(RESULT_OK, getIntent());
                    finish();
                    return;

                }
                else{
                    int code = result.getResultCode();
                    Toast.makeText(getApplicationContext(), "Error Occurred! in the bio result launcher " + code, Toast.LENGTH_LONG).show();
                    Log.i("Error_Tag", "Error in the create bio activity!!!");
                }
            }
        });




        launcher1 = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == RESULT_OK) {

                    Uri imageURI = result.getData().getData();

                    imageViews.add(imageURI);
                    Glide.with(getApplicationContext()).load(imageURI).into(image1);
                } else {

                    int code = result.getResultCode();
                    Toast.makeText(getApplicationContext(), "Error Occurred! " + code, Toast.LENGTH_LONG).show();
                    Log.i("Error_Tag", "Error!!!");
                }
            }
        });

        launcher2 = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == RESULT_OK) {

                    Uri imageURI = result.getData().getData();
                    imageViews.add(imageURI);
                    Glide.with(getApplicationContext()).load(imageURI).into(image2);
                } else {

                    int code = result.getResultCode();
                    Toast.makeText(getApplicationContext(), "Error Occurred! " + code, Toast.LENGTH_LONG).show();
                    Log.i("Error_Tag", "Error!!!");
                }
            }

        });

        launcher3 = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == RESULT_OK) {
                    Uri imageURI = result.getData().getData();

                    imageViews.add(imageURI);
                    Glide.with(getApplicationContext()).load(imageURI).into(image3);


                } else {

                    int code = result.getResultCode();
                    Toast.makeText(getApplicationContext(), "Error Occurred! " + code, Toast.LENGTH_LONG).show();
                    Log.i("Error_Tag", "Error!!!");
                }
            }
        });

        launcher4 = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == RESULT_OK) {

                    Uri imageURI = result.getData().getData();

                    imageViews.add(imageURI);
                    Glide.with(getApplicationContext()).load(imageURI).into(image4);
                } else {

                    int code = result.getResultCode();
                    Toast.makeText(getApplicationContext(), "Error Occurred! " + code, Toast.LENGTH_LONG).show();
                    Log.i("Error_Tag", "Error!!!");
                }
            }
        });

        launcher5 = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == RESULT_OK) {

                    Uri imageURI = result.getData().getData();
                    imageViews.add(imageURI);
                    Glide.with(getApplicationContext()).load(imageURI).into(image5);
                } else {

                    int code = result.getResultCode();
                    Toast.makeText(getApplicationContext(), "Error Occurred! " + code, Toast.LENGTH_LONG).show();
                    Log.i("Error_Tag", "Error!!!");
                }
            }
        });

        launcher6 = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == RESULT_OK) {

                    Uri imageURI = result.getData().getData();
                    imageViews.add(imageURI);
                    Glide.with(getApplicationContext()).load(imageURI).into(image6);
                } else {

                    int code = result.getResultCode();
                    Toast.makeText(getApplicationContext(), "Error Occurred! " + code, Toast.LENGTH_LONG).show();
                    Log.i("Error_Tag", "Error!!!");
                }
            }
        });


    }

    public void onClick(View v) {

        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        switch (v.getId()) {

            case R.id.image1:

                launcher1.launch(gallery);
                break;

            case R.id.image2:
                launcher2.launch(gallery);
                break;

            case R.id.image3:
                launcher3.launch(gallery);
                break;

            case R.id.image4:
                launcher4.launch(gallery);
                break;

            case R.id.image5:
                launcher5.launch(gallery);
                break;

            case R.id.image6:
                launcher6.launch(gallery);
                break;

            case R.id.upload:
                goToBioActivity();
        }

        if (v.getId() != R.id.upload) requestNecessaryPermission();
    }

    private void goToBioActivity() {
        //Pass our images back into the login activity
        Data d = (Data) getIntent().getSerializableExtra("data");


        Intent bioActivity = new Intent(ImageUploader.this, CreateBioActivity.class);
        bioActivity.putExtra("data", d);
        bioActivity.putExtra("imageURIs", imageViews);

        bioResultLauncher.launch(bioActivity);


    }

    public void requestNecessaryPermission() {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // You can use the API that requires the permission.

        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            // In an educational UI, explain to the user why your app requires this
            // permission for a specific feature to behave as expected. In this UI,
            // include a "cancel" or "no thanks" button that allows the user to
            // continue using your app without granting the permission.
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("This app requires permission to your files so that you may upload them to Together's database.\n"
                    + "If you decline, you will not be able to create an account and you will be taken back to the login screen.");
            builder.setNeutralButton("Ok!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    requestPermissionLauncher.launch(
                            Manifest.permission.READ_EXTERNAL_STORAGE);
                }
            });
            builder.show();
        } else {
            // You can directly ask for the permission.
            // The registered ActivityResultCallback gets the result of this request.
            requestPermissionLauncher.launch(
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }
}



