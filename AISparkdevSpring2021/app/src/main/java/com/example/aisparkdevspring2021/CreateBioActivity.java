package com.example.aisparkdevspring2021;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.aisparkdevspring2021.quizapp.QuizActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class CreateBioActivity extends AppCompatActivity {

    protected EditText biography_box;
    protected Button matchMeUp;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener firebaseAuthStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_bio);
        biography_box = (EditText) findViewById(R.id.biography);

        matchMeUp = (Button) findViewById(R.id.match_me_up);


        ActivityResultLauncher<Intent> quizResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == RESULT_OK){
                    Intent dataIntent = result.getData();
                    getIntent().removeExtra("data");
                    getIntent().putExtra("data", dataIntent.getSerializableExtra("data"));

                    setResult(RESULT_OK, getIntent());
                    finish();
                    return;
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Error Occurred!", Toast.LENGTH_LONG).show();
                }
            }
        });

        matchMeUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent quizIntent = new Intent(CreateBioActivity.this, QuizActivity.class);
                Data d = (Data) getIntent().getSerializableExtra("data");

                d.setBio(biography_box.getText().toString());
                quizIntent.putExtra("data", d);

                quizResultLauncher.launch(quizIntent);

            }
        });

        firebaseAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                Intent intent;
                if (user != null) {
                    Intent main = new Intent(CreateBioActivity.this, MainActivity.class);
                    startActivity(main);
                    finish();
                    return;
                }
            }
        };

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mAuth.addAuthStateListener(firebaseAuthStateListener);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        mAuth.removeAuthStateListener(firebaseAuthStateListener);
    }



}