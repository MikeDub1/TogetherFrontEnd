package com.example.aisparkdevspring2021.Cards;

import androidx.annotation.NonNull;

public class BackOfCards {

    private String userId, name, bio, profileImageUrl;

    public BackOfCards(String name, String bio, String profileImageUrl)
    {
        this.name = name;
        this.bio = bio;
        this.profileImageUrl = profileImageUrl;
    }

    public BackOfCards(int character) {
        switch (character) {
            case 1:
                this.name = "Vegeta";
                this.bio = "Prince of all Saiyan and enjoy fight";
                this.profileImageUrl = "src/main/java/com/example/aisparkdevspring2021/Cards/Dumby_Profile/Vegeta/vegeta1.jpg";
                break;
            case 2:
                this.name = "Sasuke Uchiha";
                this.bio = "My brother kill my clan and want to steal my eyes";
                this.profileImageUrl = "src/main/java/com/example/aisparkdevspring2021/Cards/Dumby_Profile/Sasuke Uchiha/sasuke3.jpg";
                break;
            case 3:
                this.name = "Jotaro Kujo";
                this.bio = "Just 80s punk kid raise in the 90s";
                this.profileImageUrl = "src/main/java/com/example/aisparkdevspring2021/Cards/Dumby_Profile/Jotaro Kujo/jojo1.jpg";
                break;
            case 4:
                this.name = "Black Window";
                this.bio = "Super spy and should not be trusted";
                this.profileImageUrl = "src/main/java/com/example/aisparkdevspring2021/Cards/Dumby_Profile/Black Widow/blackwidow3.jpg";
                break;
            case 5:
                this.name = "Wonder Woman";
                this.bio = "Am a strong independent woman and love to be whipped";
                this.profileImageUrl = "src/main/java/com/example/aisparkdevspring2021/Cards/Dumby_Profile/Wonder Woman/wonderwoman3.jpg";
                break;
            case 6:
                this.name = "Harley Quinn";
                this.bio = "Am a little crazy acutally borderline insane";
                this.profileImageUrl = "src/main/java/com/example/aisparkdevspring2021/Cards/Dumby_Profile/Harley Quinn/harleyquinn1.jpg";
                break;
            default:
                //no reason
        }
    }

    private void setName(String name){this.name = name;}
    private String getName(String name){return name;}

    private void setBio(String bio){this.bio = bio;}
    private String getBio(String bio){return bio;}

    private void setProfileImageUrl(String name){this.profileImageUrl = profileImageUrl;}
    private String getProfileImageUrl(String name){return profileImageUrl;}

}
