package com.example.aisparkdevspring2021;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Data implements Serializable {

    private String concatQuestions, email, password;
    private String phone, name, bio;
    private String gender, preferredGender;
    private String profileImageUrl;


    public String getPreferredGender() {
        return preferredGender;
    }

    public void setPreferredGender(String preferredGender) {
        this.preferredGender = preferredGender;
    }

    public String getProfileImageUrl() {
        return profileImageUrl != null && !profileImageUrl.isEmpty() ? profileImageUrl : "default";
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public Data(){}

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getConcatQuestions() {
        return !concatQuestions.isEmpty() ? concatQuestions: "default";
    }

    public void setConcatQuestions(String concatQuestions) {
        this.concatQuestions = concatQuestions;
    }

    public String getEmail() {
        return !email.isEmpty() ? email: "default";
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return !password.isEmpty() ? password : "default";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return !phone.isEmpty() ? phone : "default";
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return !name.isEmpty() ? name : "default";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return !bio.isEmpty() ? bio : "default";
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    @Override
    public String toString()
    {
        return "Name: " + name + "\nEmail: " + email;
    }
}
