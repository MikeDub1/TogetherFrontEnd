package com.example.aisparkdevspring2021.quizapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.text.TextWatcher;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.aisparkdevspring2021.CreateBioActivity;
import com.example.aisparkdevspring2021.Data;
import com.example.aisparkdevspring2021.ImageUploader;
import com.example.aisparkdevspring2021.R;
import com.example.aisparkdevspring2021.RegistrationActivity;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import java.util.logging.Logger;

public class QuizActivity extends AppCompatActivity {
    private TextView question, questionNumb;
    private Button option1Btn, option2Btn, option3Btn, prev, next;
    private ArrayList<Questions> questionsArrayList;
    EditText option4Btn;
    Random rand;
    int currentPos, questionInt = 1;
    private RadioGroup options;

    Stack<String> stackAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
        rand = new Random();
        question = (TextView) findViewById(R.id.Question);
        questionNumb = findViewById(R.id.qNum);
        option1Btn = findViewById(R.id.optionA);
        option2Btn = findViewById(R.id.optionB);
        option3Btn = findViewById(R.id.optionC);
        option4Btn = (EditText) findViewById(R.id.userIn);
        next = (Button) findViewById(R.id.next);
        prev = (Button) findViewById(R.id.prev);
        questionsArrayList = new ArrayList<>();
        getQuizQuestion(questionsArrayList);
        currentPos = 0;
        setDataToViews(currentPos);
        options = (RadioGroup) findViewById(R.id.options);
        stackAnswer = new Stack<String>();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int buttonId = options.getCheckedRadioButtonId();
                String ans = "";

                if (option4Btn.getText().toString().equals("")) {
                    if (buttonId == -1) {
                        Toast.makeText(getApplicationContext(), "Hey put something there!!!!", Toast.LENGTH_SHORT).show();
                        return;
                    } else {

                        RadioButton selected = (RadioButton) findViewById(buttonId);
                        ans = selected.getText().toString();
                    }
                } else {
                    ans = option4Btn.getText().toString();

                }
                stackAnswer.push(ans);
                options.clearCheck();
                option4Btn.getText().clear();

                if (currentPos >= (questionsArrayList.size() - 1))
                    gameOver();
                else {
                    questionInt++;
                    setDataToViews(++currentPos);

                }
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPos != 0) {
                    questionInt--;
                    setDataToViews(--currentPos);
                    option4Btn.getText().clear();
                    options.clearCheck();

                    if (!stackAnswer.empty()) {
                        String s = stackAnswer.pop();
                        Log.d("pop", s);
                    }



                }
            }
        });

        option4Btn.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                options.clearCheck();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO
            }
        });
    }


    private void showBottomSheet() {
        // BottomSheetDialog bottomSheetDialog
    }

    private void gameOver(){
        //HTTP request code
        String questionsAll = "";

        while(!stackAnswer.empty())
        {
            if(stackAnswer.size() == 1)
                questionsAll += stackAnswer.pop();

            else questionsAll += stackAnswer.pop() + "|||";
        }

        Data d = (Data) getIntent().getSerializableExtra("data");
        d.setConcatQuestions(questionsAll);

        getIntent().removeExtra("data");
        getIntent().putExtra("data", d);





        setResult(RESULT_OK, getIntent());
        finish();
        return;

    }


    private void setDataToViews(int CurrentPos) {
        questionNumb.setText("Question : " + questionInt);

        question.setText(questionsArrayList.get(currentPos).getQuestionText());
        option1Btn.setText(questionsArrayList.get(currentPos).getOptA());
        option2Btn.setText(questionsArrayList.get(currentPos).getOptB());
        option3Btn.setText(questionsArrayList.get(currentPos).getOptC());
    }


    private void goToCreateBioActivity() {
        Intent myIntent = getIntent();

        String email = myIntent.getStringExtra("email");
        String password = myIntent.getStringExtra("password");
        String name = myIntent.getStringExtra("name");
        String radioButtonText = myIntent.getStringExtra("button");

        Intent createBio = new Intent(QuizActivity.this, CreateBioActivity.class);

        createBio.putExtra("email", email);
        createBio.putExtra("password", password);
        createBio.putExtra("name", name);
        createBio.putExtra("button", radioButtonText);

        startActivity(createBio);
        finish();
        return;
    }


    private void getQuizQuestion(ArrayList<Questions> quizActivityArrayList) {
        quizActivityArrayList.add(new Questions("What are your interests?", "Video Games", "Partying/Dancing", "Fitness/Sports"));
        quizActivityArrayList.add(new Questions("What do you do for work?", "Tech", "Medical", "An art"));
        quizActivityArrayList.add(new Questions("Do you consider yourself a shy person or out going person?", "No, why tf are you so close to me", "Yes, I am a social butterfly", "Kinda, every now and then I like hit the clubs, but not every weekend"));
        quizActivityArrayList.add(new Questions("What is your star sign??", "Scorpio", "Aquarius", "Cancer"));
        quizActivityArrayList.add(new Questions("How do you normally spent your weekend?", "I enjoy long walks on the beach.", "Space baby!!!", "Netflix and chill."));
        quizActivityArrayList.add(new Questions("What are your favorite places to hangout?", "Hangout? Is that something you eat?", "Wherever there is a party!", "My house."));
        quizActivityArrayList.add(new Questions("Are you physically active and if you do what kinds?", "I am a professional sleeper.", "I am a professional bodybuilder.", "I am a professional gamer."));
        quizActivityArrayList.add(new Questions("What is the most recent place you visited?", "My bathroom", "Latin America", "Europe"));


    }
}