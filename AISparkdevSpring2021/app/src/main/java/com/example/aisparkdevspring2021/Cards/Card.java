package com.example.aisparkdevspring2021.Cards;

public class Card {
    private String userId;
    private String name;
    private String profileImageUrl;
    private String bio;

    public Card(String userID, String name, String profileImageUrl, String bio, int character){
        this.userId = userID;
        this.name = name;
        this.profileImageUrl = profileImageUrl;
        this.bio = bio;
        switch (character) {
            case 1:
                this.name = "Vegeta";
                this.bio = "Prince of all Saiyan and enjoy fight";
                this.profileImageUrl = "file:///Dumby_Profile/Vegeta/vegeta1.jpg";
                break;
            case 2:
                this.name = "Sasuke Uchiha";
                this.bio = "My brother kill my clan and want to steal my eyes";
                this.profileImageUrl = "file:///Dumby_Profile/Sasuke Uchiha/sasuke3.jpg";
                break;
            case 3:
                this.name = "Jotaro Kujo";
                this.bio = "Just 80s punk kid raise in the 90s";
                this.profileImageUrl = "file:///Dumby_Profile/Dumby_Profile/Jotaro Kujo/jojo1.jpg";
                break;
            case 4:
                this.name = "Black Window";
                this.bio = "Super spy and should not be trusted";
                this.profileImageUrl = "file:///Dumby_Profile/Cards/Dumby_Profile/Black Widow/blackwidow3.jpg";
                break;
            case 5:
                this.name = "Wonder Woman";
                this.bio = "Am a strong independent woman and love to be whipped";
                this.profileImageUrl = "file:///Dumby_Profile/Dumby_Profile/Wonder Woman/wonderwoman3.jpg";
                break;
            case 6:
                this.name = "Harley Quinn";
                this.bio = "Am a little crazy acutally borderline insane";
                this.profileImageUrl = "file:///Dumby_Profile/Dumby_Profile/Harley Quinn/harleyquinn1.jpg";
                break;
            default:
                //no reason
        }
    }



    public String getUserId(){
        return userId;
    }
    public void setUserID(String userID){
        this.userId = userId;
    }

    public String getName(){
        return name;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public void setName(String name){
        this.name = name;
    }

    private void setBio(String bio){this.bio = bio;}
    private String getBio(String bio){return bio;}
}